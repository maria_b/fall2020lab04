package geomentry;
import java.lang.Math;
//Maria Barba 1932657
public class Rectangle implements Shape {
private double length;
private double width;


public Rectangle (double length,double width) {
this.length=length;
this.width=width;

}
public double getLength() {
return this.length;	
}
public double getWidth() {
return this.width;
}
public double getArea() {
	double area=getLength()*getWidth();
	return area;
}
	
public double getPerimeter() {
 double perimeter = (getLength()+getWidth())*2;
 return perimeter;
}
}
