	package geomentry;
	//Maria Barba 1932657
	
	public class LotsOfShapes {
	
		public static void main(String[] args) {
			Shape[] myShapes = new Shape[5];
			myShapes[0]=new Rectangle(2.5, 3.0);
			myShapes[1]=new Rectangle(5.6 ,4.0);
			myShapes[2]=new Circle(1.0);
			myShapes[3]=new Circle(4.5);
			myShapes[4]=new Square(3.0);
		
			for(int i=0 ;i<myShapes.length;i++) {
				
					System.out.println("area :"+ myShapes[i].getArea() + " perimeter :"+myShapes[i].getPerimeter());
				
			}
		}
	
	}
