package geomentry;
import java.lang.Math;
//Maria Barba 1932657
public class Circle implements Shape {

private double radius;

public Circle(double radius) {
this.radius=radius;
}

public double getRadius() { 
    return this.radius; 
}
	
public double getArea() {
	double pi=Math.PI;
	double area=  Math.pow(getRadius(),2)*pi;
	return area;
}
	
public double getPerimeter() {
 double pi=Math.PI;
 double diameter=getRadius()*2;
 double perimeter = pi*diameter;
 return perimeter;
}
}
