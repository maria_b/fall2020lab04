package inheritance;


//Maria Barba 1932657
public class ElectronicBook extends Book{
private int numberBytes;

public static void main(String [] args) {
	ElectronicBook myBook =new ElectronicBook("Alice in Wonderland","Some Author",3);
	System.out.println(myBook.toString());
}
public ElectronicBook (String title,String author, int numberBytes) {
	super(title,author);
	
	this.numberBytes=numberBytes;
}
public String toString() {
    String fromBase = super.toString();
    return (fromBase + " numberBytes : "+ numberBytes);
}



}
