package inheritance;
//Maria Barba 1932657
public class Bookstore {
	public static void main(String [] args) {
		Book[] myBooksArray = new Book[5];
		myBooksArray[0] = new Book("Alice in Wonderland","Some Author");
		myBooksArray[1] = new ElectronicBook("Book2","Author2",3);
		myBooksArray[2] = new Book("Book3","Author3");
		myBooksArray[3] = new ElectronicBook("Book4","Author4",5);
		myBooksArray[4] = new ElectronicBook("Book5","Author5",5);
		
		for(int i=0; i< myBooksArray.length; i++) {
			System.out.println(myBooksArray[i].toString());
		}
	}

}
